package fr.miage.game

import fr.miage.view.MainView
import java.util.*
import kotlin.collections.HashSet
/**
 * Board of the game
 * This class contains all the logic about the cells
 * It only knows the state of the board
 */
class Board(val dimension: Int, nbAlive: Int) {
    var livingCells: MutableList<Cell> = mutableListOf()


    /**
     * Init the board with the number of living cell in parameter
     * Generate random coordinate for these cells
     * and add it to the livingCells list
     */
    init {
        val random = Random()
        var bool: Boolean = true
        var c = Cell(0, 0)
        (0 until nbAlive).forEach {
            c = Cell(random.nextInt(dimension), random.nextInt(dimension))
            if (livingCells.isEmpty()) {
                livingCells.add(c)
            } else if (!livingCells.contains(c)) {
                livingCells.add(c)
            } else {
                while (livingCells.contains(c)) {
                    c = Cell(random.nextInt(dimension), random.nextInt(dimension))
                }
                livingCells.add(c)
            }

        }
    }


    /**
     * Check if a cell(i,j) is alive
     *
     * @param x column index of a cell
     * @param y row index of a cell
     *
     * @return if the cell in parameter is alive (true) or dead (false)
     */
    fun isAlive(x: Int, y: Int): Boolean {
        return livingCells.contains(x to y)
    }


    /**
     * Extension to calculate the difference between a cell and another one
     */
    operator fun Cell.minus(other: Cell): Cell {
        return Cell(this.first + other.first, this.second + other.second)
    }


    /**
     * This function describes the logic of the game.
     * It determines if a cell will live or not
     *
     * @param nbAliveNeighbors number of alive neighbors
     * @param currentCellIsAlive current state of the cell (true if it is alive - else false)
     *
     * @return the new state of the cell - true if it alive - else false
     */
    fun getNextState(nbAliveNeighbors: Int, currentCellIsAlive: Boolean): Boolean =
            when (nbAliveNeighbors) {
                0, 1 -> false
                2 -> currentCellIsAlive
                3 -> true
                else -> false
            }


    /**
     * Return the neighbors of a cell as described :
     * (-1,-1)(-1,0)(-1,1)
     * (0,-1) Cell(0,0) (0,1)
     * (1,-1)(1,0)(1,1)
     *
     * @param cell the cell to count neighbors from
     *
     * @return the Cells list of neighbors
     */
    fun getNeighbors(cell: Cell): List<Cell> =
        (-1..1).flatMap { x ->
            (-1..1).map { y ->
                cell-(x to y)
            }
        }-cell



    /**
     * Run an iteration of the game of life
     */
    fun evolve() {
        var newStateCells: MutableList<Cell> = mutableListOf()
        (0 until dimension).flatMap { x ->
            (0 until dimension).map { y ->

                val nbNeighborsAlive = getNeighbors((x to y)).filter {
                    isAlive(it.first, it.second)
                }.size
                if (getNextState(nbNeighborsAlive, isAlive(x, y))) {
                    newStateCells.add(Cell(x, y))
                }

            }
        }
        livingCells = newStateCells
    }
}






/**
 * We create a alias only for readability instead of using the Pair<Int,Int> object
 * With that you can use Cell as a Pair<Int,Int>
 */
typealias Cell = Pair<Int, Int>
